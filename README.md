java8-hands-on-jfall-2014
=========================

# Java8 hands-on workshop for JFall 2014

## Before starting

In order to prevent making changes to your system and if you want to use the Maven build you need to specify the
location of the Java 8 compiler in the pom.xml.
This can be done by adding the property to your own `settings.xml` or use the `settings.xml` found in the root of the workshop directory. Or you can change the location directly in the `pom.xml`.

## Coding

The exercises can be found in the exercises project, you only need to change source code in src/main/java.
In order to compile and run the testcases use: `mvn test`


## Developers

Always add the following profile to your Maven command `-Pall`.

### New exercises

New exercises and lessons must be created in the project `solutions`

### Publish

* In project solutions, type: `mvn clean install` this will run all the testcases
* Type `mvn clean install -Dmaven.test.skip -Pcreate_exercises -Pall` this will precompile the solutions in order to create the exercises and will copy the directories to the project `exercises`
* Remove all the unused imports in the exercises project
* Check-in the code including the exercises project.

### Update the project dependencies

Because access to the Internet is not guaranteed we create a jar file called `offline.jar` which create all the
necessary dependencies to run the exercises. If you need a extra dependency add the dependency to the project
`shared` and run the following command `mvn clean install -Pdependency_rebuild -Psolution`
The jar file will be copied to the project parent directory.

To validate the preprocessor of the solutions you can change the value in the pom:

``
<property>
  <name>TARGET</name>
  <value>ASSIGNMENT</value>
</property>
``

And change the variable to SOLUTION.